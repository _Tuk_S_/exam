<?php

namespace AppBundle\Repository;

/**
 * ReviewRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ReviewRepository extends \Doctrine\ORM\EntityRepository
{
    public function findRating($id)
    {
        return $this
            ->createQueryBuilder('review')
            ->select('review.qualityKitchen', 'review.qualityService', 'review.situation')
            ->where('review.institution = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function checksNullReview($id, $user)
    {
        return $this->createQueryBuilder('review')
            ->andWhere('review.institution = :institution')
            ->andwhere('review.user = :user')
            ->setParameter('user', $user)
            ->setParameter('institution', $id)
            ->getQuery()
            ->getResult();
    }
}
