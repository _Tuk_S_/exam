<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Review
 *
 * @ORM\Table(name="review")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReviewRepository")
 */
class Review
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="review", type="text")
     */
    private $review;
    /**
     * @var integer
     *
     * @ORM\Column(name="qualityKitchen", type="integer")
     */
    private $qualityKitchen;

    /**
     * @var integer
     *
     * @ORM\Column(name="qualityService", type="integer")
     */
    private $qualityService;

    /**
     * @var integer
     *
     * @ORM\Column(name="situation", type="integer")
     */
    private $situation;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Institution", inversedBy="reviews")
     */
    private $institution;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="reviews")
     */
    private $user;

    public function setInstitution(Institution $institution)
    {
        $this->institution = $institution;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getInstitution()
    {
        return $this->institution;
    }

    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set review
     *
     * @param string $review
     *
     * @return Review
     */
    public function setReview($review)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return string
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set qualityKitchen
     *
     * @param integer $qualityKitchen
     *
     * @return Review
     */
    public function setQualityKitchen($qualityKitchen)
    {
        $this->qualityKitchen = $qualityKitchen;

        return $this;
    }

    /**
     * Get qualityKitchen
     *
     * @return integer
     */
    public function getQualityKitchen()
    {
        return $this->qualityKitchen;
    }

    /**
     * Set qualityService
     *
     * @param integer $qualityService
     *
     * @return Review
     */
    public function setQualityService($qualityService)
    {
        $this->qualityService = $qualityService;

        return $this;
    }

    /**
     * Get qualityService
     *
     * @return integer
     */
    public function getQualityService()
    {
        return $this->qualityService;
    }

    /**
     * Set situation
     *
     * @param integer $situation
     *
     * @return Review
     */
    public function setSituation($situation)
    {
        $this->situation = $situation;

        return $this;
    }

    /**
     * Get situation
     *
     * @return integer
     */
    public function getSituation()
    {
        return $this->situation;
    }

    public function __toString()
    {
        return $this->review ?: '';
    }


}
