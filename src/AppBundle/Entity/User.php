<?php
namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Picture", mappedBy="user")
     */
    private $pictures;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Review", mappedBy="user")
     */
    private $reviews;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Institution", mappedBy="user")
     */
    private $institutions;

    public function __construct()
    {
        parent::__construct();
        $this->pictures = new ArrayCollection();
        $this->reviews = new ArrayCollection();
        $this->institutions = new ArrayCollection();
    }


    public function getPictures()
    {
        return $this->pictures;
    }


    public function getInstitution()
    {
        return $this->institutions;
    }

    public function getReviews()
    {
        return $this->reviews;
    }

    public function __toString()
    {
        return $this->username ?: '';
    }


    /**
     * Add picture
     *
     * @param \AppBundle\Entity\Picture $picture
     *
     * @return User
     */
    public function addPicture(\AppBundle\Entity\Picture $picture)
    {
        $this->pictures[] = $picture;

        return $this;
    }

    /**
     * Remove picture
     *
     * @param \AppBundle\Entity\Picture $picture
     */
    public function removePicture(\AppBundle\Entity\Picture $picture)
    {
        $this->pictures->removeElement($picture);
    }

    /**
     * Add review
     *
     * @param \AppBundle\Entity\Review $review
     *
     * @return User
     */
    public function addReview(\AppBundle\Entity\Review $review)
    {
        $this->reviews[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \AppBundle\Entity\Review $review
     */
    public function removeReview(\AppBundle\Entity\Review $review)
    {
        $this->reviews->removeElement($review);
    }

    /**
     * Add institution
     *
     * @param \AppBundle\Entity\Institution $institution
     *
     * @return User
     */
    public function addInstitution(\AppBundle\Entity\Institution $institution)
    {
        $this->institutions[] = $institution;

        return $this;
    }

    /**
     * Remove institution
     *
     * @param \AppBundle\Entity\Institution $institution
     */
    public function removeInstitution(\AppBundle\Entity\Institution $institution)
    {
        $this->institutions->removeElement($institution);
    }

    /**
     * Get institutions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInstitutions()
    {
        return $this->institutions;
    }
}
