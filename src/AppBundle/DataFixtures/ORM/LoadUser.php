<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadUser implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();

        $user
            ->setEmail('fgh@com.ru')
            ->setUsername('Jonn')
            ->setRoles(['ROLE_USER'])
            ->setEnabled(true);
        $encoder = $this->container->get('security.password_encoder');
        $password1 = $encoder->encodePassword($user, '123');
        $user->setPassword($password1);
        $manager->persist($user);

        $manager->flush();
    }
}
