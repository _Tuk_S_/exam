<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Institution;
use AppBundle\Entity\Picture;
use AppBundle\Entity\Review;
use AppBundle\Form\InstitutionType;
use AppBundle\Form\PictureType;
use AppBundle\Form\ReviewType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class InstitutionController extends Controller
{
    /**
     * @Route("/newInstitution")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function newInstitutionAction(Request $request)
    {
        $institution = new Institution();
        $form = $this->createForm(InstitutionType::class, $institution);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $institution->setUser($this->getUser());
            $institution = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($institution);
            $em->flush();

            return $this->redirectToRoute("app_institution_message");
        }
        return $this->render('AppBundle:Institution:newInstitution.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/massage")
     */
    public function messageAction()
    {
        return $this->render('AppBundle:Institution:message.html.twig', array());
    }

    /**
     * @Route("/institution/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD", "POST"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function institutionAction(Request $request, int $id)
    {
        $institution = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->find($id);

        $review = new Review();
        $form = $this->createForm(ReviewType::class, $review);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $review->setInstitution($institution);
            $review->setUser($this->getUser());
            $review = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($review);
            $em->flush();

            return $this->redirectToRoute("app_institution_institution", [
                'id' => $id,
            ]);
        }

        return $this->render('AppBundle:Institution:institution.html.twig', array(
            'institution' => $institution,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/addRevew/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD", "POST"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addReviewAction(Request $request, int $id)
    {

        $review_user = $this->getDoctrine()
            ->getRepository('AppBundle:Review')
            ->checksNullReview($id, $this->getUser()->getId());

        if ($review_user != null) {
            return $this->render('AppBundle:Institution:delete.html.twig', [
                'review' => $review_user,
            ]);
        }

        $institution = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->find($id);

        $review = new Review();
        $form = $this->createForm(ReviewType::class, $review);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $review->setInstitution($institution);
            $review->setUser($this->getUser());
            $review = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($review);
            $em->flush();

            return $this->redirectToRoute("app_institution_institution", [
                'id' => $id,
            ]);
        }

        return $this->render('AppBundle:Institution:addReview.html.twig', array(
            'institution' => $institution,
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/deleteReview/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD", "POST"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteReviewAction(Request $request, int $id)
    {
        $review = $this->getDoctrine()
            ->getRepository('AppBundle:Review')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($review);
        $em->flush();


        return $this->redirectToRoute("homepage", []);
    }


    /**
     * @Route("/addPicture/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD", "POST"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addPictureAction(Request $request, int $id)
    {
        $institution = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->find($id);

        $picture = new Picture();
        $form = $this->createForm(PictureType::class, $picture);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $picture->setInstitution($institution);
            $picture->setUser($this->getUser());
            $picture = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($picture);
            $em->flush();

            return $this->redirectToRoute("app_institution_institution", [
                'id' => $id,
            ]);
        }

        return $this->render('AppBundle:Institution:addPicture.html.twig', array(
            'institution' => $institution,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/category/{id}", requirements={"id": "\d+"})
     * @Method({"GET","HEAD", "POST"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryAction(Request $request, int $id)
    {
        $category = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->find($id);

        return $this->render('AppBundle:Institution:category.html.twig', array(
            'category' => $category,
        ));
    }

    /**
     * @Route("/findInstitution")
     * @Method({"GET","HEAD", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function findInstitutionAction(Request $request)
    {
        $institution = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->findSearch($request->request->get("title"));
        return $this->render('AppBundle:Institution:findInstitution.html.twig', array(
            'institution' => $institution,
        ));
    }
}
