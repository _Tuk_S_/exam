<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $categories = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findAll();
        $institutions = $this->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->findBy(['publ' => true]);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $institutions,
            $request->query->getInt('page', 1)/*page number*/,
            20/*limit per page*/
        );

        return $this->render('default/index.html.twig', [
            'categories' => $categories,
            'institutions' => $institutions,
            'pagination' => $pagination
        ]);
    }


    /**
     * @Route("/rating/{id}")
     */
    public function ratingAction(Request $request, $id)
    {

        $reviews = $this->getDoctrine()
            ->getRepository('AppBundle:Review')
            ->find($id);
        $array = [
            $reviews->getQualityKitchen(),
            $reviews->getQualityService(),
            $reviews->getSituation(),

        ];

        $result = array_sum($array);
        return new JsonResponse($result);
    }
}
